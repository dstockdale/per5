Variant.class_eval do
  def options_list
    self.option_values.sort{|ov1, ov2| ov1.option_type.position <=> ov2.option_type.position}.map { |ov|
      obj = OpenStruct.new
      obj.label = ov.option_type.presentation
      obj.value = ov.presentation
      obj
    }
  end
end