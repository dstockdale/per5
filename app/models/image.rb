class Image < Asset
  validate :no_attachement_errors
  has_attached_file :attachment, 
    :styles => { :mini => '92x92#', :small => '185x185#', :product => '383x383#', :large => '800x800>' },
    :default_style => :product,
    # :path => "/assets/products/:id/:style/:basename_:timestamp.:extension",
    :path => "/assets/products/:id/:style/:basename.:extension",
    :storage => 's3',
    :s3_credentials => Rails.root.join('config', 's3.yml'),
    :s3_host_alias => "cdn#{rand(3)}.per5.co.uk",
    :url => ':s3_alias_url' 

  # save the w,h of the original image (from which others can be calculated)
  # we need to look at the write-queue for images which have not been saved yet
  after_post_process :find_dimensions

  def find_dimensions
    temporary = attachment.queued_for_write[:original] 
    filename = temporary.path unless temporary.nil?
    filename = attachment.path if filename.blank?
    geometry = Paperclip::Geometry.from_file(filename)
    self.attachment_width  = geometry.width
    self.attachment_height = geometry.height
  end

  # if there are errors from the plugin, then add a more meaningful message
  def no_attachement_errors
    unless attachment.errors.empty?
      # uncomment this to get rid of the less-than-useful interrim messages
      # errors.clear 
      errors.add :attachment, "Paperclip returned errors for file '#{attachment_file_name}' - check ImageMagick installation or image source file."
      false
    end
  end
end
# == Schema Information
#
# Table name: assets
#
#  id                      :integer         not null, primary key
#  viewable_id             :integer
#  viewable_type           :string(50)
#  attachment_content_type :string(255)
#  attachment_file_name    :string(255)
#  attachment_size         :integer
#  position                :integer
#  type                    :string(75)
#  attachment_updated_at   :datetime
#  attachment_width        :integer
#  attachment_height       :integer
#  alt                     :text
#

