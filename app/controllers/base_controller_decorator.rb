Spree::BaseController.class_eval do
  before_filter :load_root_taxons, :check_uri

  private
  
  def load_root_taxons
    @taxons ||= Taxonomy.where(:name => "Range").first.root.children
  end

  def check_uri
    if Rails.env == 'production'
      redirect_to request.protocol + "www." + request.host_with_port + request.request_uri if !/^www/.match(request.host)
    end
  end

end