class MessagesController < Spree::BaseController

  # GET /thankyou
  def show
    respond_to do |wants|
      wants.html # show.html.erb
    end
  end

  # GET /contact
  def new
    @message = Message.new
    respond_to do |wants|
      wants.html # new.html.erb
    end
  end
  
  # POST /messages
  def create
    @message = Message.new(params[:message])
    if @message.valid?
      MessageMailer.contact_form(@message).deliver
      MessageMailer.confirm_sent(@message).deliver
      flash[:notice] = "Message sent! Thank you for contacting us."
      redirect_to contact_url
    else
      render :action => 'new'
    end
  end
  
end
