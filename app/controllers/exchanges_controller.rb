class ExchangesController < Spree::BaseController
  
  def show
    amount = params[:amount].gsub(/[^\d.]+/,'').to_f
    to = params[:currency]
    query ="1GBP=?#{to}"
    url = "http://www.google.com/ig/calculator?hl=en&q=#{query}"
    result = Net::HTTP.get_response(URI.parse(url)).body.split('"')
    current_rate = (result[3].split(' ')[0]).to_f
    conversion = (current_rate * amount).to_f.round(2)
    rhs = result[3].gsub(result[3].split(' ')[0],'')

    respond_to do |wants|
      wants.js do 
        render :json => { 
                  :converted => "#{conversion}",
                  :rhs => rhs,
                  :rate => "#{current_rate}", 
                  :to_the_pound => "#{current_rate.round(2)}"
                }, :status => :ok
      end
    end
  end
  
end