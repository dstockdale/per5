module ApplicationHelper
  
  def is_controller?(controller, action)
    controller == params[:controller] && action == params[:action]
  end

  def nav_title(product)
    product.nav_title.present? ? product.nav_title : product.name
  end
  
end