CheckoutHelper.module_eval do 
  def billing_used_class(order)
    billing = (!(order.bill_address.empty? && order.ship_address.empty?) && order.bill_address.eql?(@order.ship_address))
    billing ? " usesBilling" : nil
  end
end