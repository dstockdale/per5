class MessageMailer < ActionMailer::Base
  default :from => "PER5 Clothing <sales@per5.co.uk>"
  
  def contact_form(message)
    @message = message
    mail(:to => "PER5 Clothing <sales@per5.co.uk>", :from => "#{@message.name} <#{@message.email}>", :subject => "PER5 Contact Form")
  end
  
  def confirm_sent(message)
    @message = message
    mail(:to => "#{@message.name} <#{@message.email}>", :subject => "PER5 Clothing: Thanks for your message")
  end

end