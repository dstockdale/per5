(($) ->
  $(document).ready ->

    $('#banner').waitForImages ->
      $(@).removeClass('hideArrows')

    $("#showcase").awShowcase 
      content_width: 955
      content_height: 500
      buttons: false
      auto: true
      pauseonover: true
      interval: 3000
      continuous: true
      stoponclick: false
      transition_speed: 800
    
    $.facebox.settings.closeImage = '/images/facebox/closelabel.png'
    $.facebox.settings.loadingImage = '/images/facebox/loading.gif'
    
    $(document).bind 'reveal.facebox', ()->
      amount = $('p.prices span.price').text()
      $('#facebox').find('select,button,input').uniform()
      $('#convertAmount').text(amount)
      $('#closeFacebox').live 'click', ->
        $(document).trigger('close.facebox')
      $('#submitAddToCart').live 'click', ->
        $('#cart-form .addToCartButton').click()
    
    $('a[rel=facebox]').facebox()
    
    $('.currencyLookup').click (e)->
      e.preventDefault()
      amount = $('p.prices').find('span.price').text()
      $.facebox
        ajax : '/currencies.html'
          
    $('.navigation-list > li a').dblclick (e)->
      e.stopPropagation()
      window.document.location.href = $(@).attr('href')
    
    $('.navigation-list > li h3').click (e)->
      e.preventDefault()
      $(@).parent('li').siblings('li').removeClass('active').find('ul').slideUp().removeClass('active')
      $(@).parent('li').find('ul').slideDown()
    
    $('#convert').live "click", (e)->
      e.preventDefault()
      currency = $('#toCurrency').val()
      amount = $('#convertAmount').text()
      $.ajax
        url: "/exchange",
        type: 'get',
        dataType: 'json',
        data: "currency=#{currency}&amount=#{amount}",
        beforeSend: ()->
          $('#coversionResult').html("<span class='beforeSend'>Fetching rates from http://www.google.com/finance/converter</span>")
        success: (data)->
          response = "<span class='convertedPrice'>#{data.converted} #{data.rhs}</span>"
          $('#coversionResult').html(response)
        error: (jqXHR, textStatus, errorThrown)->
          $('#coversionResult').text("Oops there was an error! Please try clicking 'Convert' again. Sometimes the server might be busy.")
    
    $('#messages').find('input, button, textarea').uniform()
    $('#checkout_form_registration').find('input, button').uniform()
    $('#cart-form').find('select,input').not('#option_values_12_3').uniform()
    $('#billing input, #shipping input, #order_submit').uniform()
    $('#login, #cart').find('input,button,a.button').uniform()
    
    if $('#flashMessage,#flashErrorMessage').length > 0
      setTimeout( ->
        $('#flashMessage,#flashErrorMessage').css(
          'overflow':'hidden'
        ).animate(
          width: 120,
          height: 120,
          'margin-left': "+=115"
        , 2000).animate(
          height: 10
        , 1000).fadeOut()
      , 5000);
    
    # $("input, textarea, select, button").uniform()
    $("form#updatecart a.delete").show().live "click", (e) ->
      $(@).parents("tr").find("input.line_item_quantity").val 0
      $(@).parents("form").submit()
      e.preventDefault()
      return
    return
  return
) jQuery