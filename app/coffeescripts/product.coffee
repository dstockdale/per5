add_image_handlers = ->
  $("#main-image").data "selectedThumb", $("#main-image img").attr("src")
  $("ul.thumbnails li").eq(0).addClass "selected"
  $("ul.thumbnails li a").click(->
    $("#main-image").data "selectedThumb", $(this).attr("href")
    $("ul.thumbnails li").removeClass "selected"
    $(this).parent("li").addClass "selected"
    false
  ).hover (->
    $("#main-image img").attr "src", $(this).attr("href").replace("mini", "product")
    return
  ), ->
    $("#main-image img").attr "src", $("#main-image").data("selectedThumb")
    return
    
jQuery(document).ready ->
  add_image_handlers()
  return

jQuery(document).ready ->
  
  window.shirtStyleTemplate = '''
  <div class="shirt_styles_section">
    <div class="shirt_styles_title">
      Choose Style
    </div>
    <div id="shirt_styles" class="shirt_styles shirtStyle_white">
      <div id="vest" class="vest selected"></div>
      <div id="racer_back" class="racer_back"></div>
      <div id="half_sleeve" class="half_sleeve"></div>
    </div>
  </div>
  '''
  window.shirtStyles = {}
  window.shirtColours = {}
  window.shirtColoursArr = []
  
  # jQuery("input, textarea, select, button").uniform()

  if jQuery("#shirt_styles").length < 1
    jQuery("#option-type_shirt_style_womens").before(window.shirtStyleTemplate)
  
  $('#option-type_shirt_style_womens option').each (i,item)->
    $item = $(item)
    window.shirtStyles[$item.text().toLowerCase().replace(/\s/g,'_')] = $item.val()
    return
  
  $('div[id^="option-type_colour_"]').find('option').each (i,item)->
    $item = $(item)
    string = $item.text().toLowerCase().split(' ')[0]
    value = $item.val()
    window.shirtColours[string] = value
    window.shirtColoursArr[value] = string
    return
    
  styleColours = ->
    selectedColour = 'shirtStyle_' + window.shirtColoursArr[$('div[id^="option-type_colour_"]').find('option:selected').val()]
    $('#shirt_styles').removeClass('shirtStyle_white shirtStyle_black').addClass(selectedColour)
    return
    
  $('div[id^="option-type_colour_"]').find('select').change ->
    styleColours()
    return
  
  styleColours()
    
  $('#shirt_styles > div').click ->
    $(@).siblings().removeClass('selected')
    $(@).toggleClass('selected')
    choice = $(this).attr('id');
    $('#option-type_shirt_style_womens select').val(window.shirtStyles[choice])
    $('#option-type_shirt_style_womens').change()
    return
    
  jQuery("#product-variants input[type=radio]").click (event) ->
    vid = @value
    text = $(this).siblings(".variant-description").html()
    jQuery("#variant-thumbnails").empty()
    jQuery("#variant-images span").html text
    if images[vid].length > 0
      $.each images[vid], (i, link) ->
        jQuery("#variant-thumbnails").append "<li>" + link + "</li>"
        return
      jQuery("#variant-images").show()
    else
      jQuery("#variant-images").hide()
    add_image_handlers()
    link = jQuery("#variant-thumbnails a")[0]
    jQuery("#main-image img").attr src: jQuery(link).attr("href")
    jQuery("ul.thumbnails li").removeClass "selected"
    jQuery(link).parent("li").addClass "selected"
        
    
    
    