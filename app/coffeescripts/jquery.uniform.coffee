(($) ->
  $.uniform = 
    options: 
      selectClass: "selector"
      radioClass: "radio"
      checkboxClass: "checker"
      fileClass: "uploader"
      filenameClass: "filename"
      fileBtnClass: "action"
      fileDefaultText: "No file selected"
      fileBtnText: "Choose File"
      checkedClass: "checked"
      focusClass: "focus"
      disabledClass: "disabled"
      buttonClass: "button"
      activeClass: "active"
      hoverClass: "hover"
      useID: true
      idPrefix: "uniform"
      resetSelector: false
      autoHide: true
    
    elements: []
  
  if $.browser.msie and $.browser.version < 7
    $.support.selectOpacity = false
  else
    $.support.selectOpacity = true
  $.fn.uniform = (options) ->
    doInput = (elem) ->
      $el = $(elem)
      $el.addClass $el.attr("type")
      storeElement elem
    doTextarea = (elem) ->
      $(elem).addClass "uniform"
      storeElement elem
    doButton = (elem) ->
      $el = $(elem)
      divTag = $("<div>")
      spanTag = $("<span>")
      divTag.addClass options.buttonClass
      divTag.attr "id", options.idPrefix + "-" + $el.attr("id")  if options.useID and $el.attr("id") != ""
      
      if $el.is("a") or $el.is("button")
        btnText = $el.text()
      else btnText = $el.attr("value")  if $el.is(":submit") or $el.is(":reset") or $el.is("input[type=button]")
      btnText = (if btnText == "" then (if $el.is(":reset") then "Reset" else "Submit") else btnText)
      spanTag.html btnText
      $el.css "opacity", 0
      $el.wrap divTag
      $el.wrap spanTag
      divTag = $el.closest("div")
      spanTag = $el.closest("span")
      divTag.addClass options.disabledClass  if $el.is(":disabled")
      divTag.bind 
        "mouseenter.uniform": ->
          divTag.addClass options.hoverClass
        
        "mouseleave.uniform": ->
          divTag.removeClass options.hoverClass
          divTag.removeClass options.activeClass
        
        "mousedown.uniform touchbegin.uniform": ->
          divTag.addClass options.activeClass
        
        "mouseup.uniform touchend.uniform": ->
          divTag.removeClass options.activeClass
        
        "click.uniform touchend.uniform": (e) ->
          if $(e.target).is("span") or $(e.target).is("div")
            if elem[0].dispatchEvent
              ev = document.createEvent("MouseEvents")
              ev.initEvent "click", true, true
              elem[0].dispatchEvent ev
            else
              elem[0].click()
      
      elem.bind 
        "focus.uniform": ->
          divTag.addClass options.focusClass
        
        "blur.uniform": ->
          divTag.removeClass options.focusClass
      
      $.uniform.noSelect divTag
      storeElement elem
    doSelect = (elem) ->
      $el = $(elem)
      divTag = $("<div />")
      spanTag = $("<span />")
      divTag.hide()  if not $el.css("display") == "none" and options.autoHide
      divTag.addClass options.selectClass
      divTag.attr "id", options.idPrefix + "-" + elem.attr("id")  if options.useID and elem.attr("id") != ""
      selected = elem.find(":selected:first")
      selected = elem.find("option:first")  if selected.length == 0
      spanTag.html selected.html()
      elem.css "opacity", 0
      elem.wrap divTag
      elem.before spanTag
      divTag = elem.parent("div")
      spanTag = elem.siblings("span")
      elem.bind 
        "change.uniform": ->
          spanTag.text elem.find(":selected").html()
          divTag.removeClass options.activeClass
        
        "focus.uniform": ->
          divTag.addClass options.focusClass
        
        "blur.uniform": ->
          divTag.removeClass options.focusClass
          divTag.removeClass options.activeClass
        
        "mousedown.uniform touchbegin.uniform": ->
          divTag.addClass options.activeClass
        
        "mouseup.uniform touchend.uniform": ->
          divTag.removeClass options.activeClass
        
        "click.uniform touchend.uniform": ->
          divTag.removeClass options.activeClass
        
        "mouseenter.uniform": ->
          divTag.addClass options.hoverClass
        
        "mouseleave.uniform": ->
          divTag.removeClass options.hoverClass
          divTag.removeClass options.activeClass
        
        "keyup.uniform": ->
          spanTag.text elem.find(":selected").html()
      
      divTag.addClass options.disabledClass  if $(elem).attr("disabled")
      $.uniform.noSelect spanTag
      storeElement elem
    doCheckbox = (elem) ->
      $el = $(elem)
      divTag = $("<div />")
      spanTag = $("<span />")
      divTag.hide()  if not $el.css("display") == "none" and options.autoHide
      divTag.addClass options.checkboxClass
      divTag.attr "id", options.idPrefix + "-" + elem.attr("id")  if options.useID and elem.attr("id") != ""
      $(elem).wrap divTag
      $(elem).wrap spanTag
      spanTag = elem.parent()
      divTag = spanTag.parent()
      $(elem).css("opacity", 0).bind 
        "focus.uniform": ->
          divTag.addClass options.focusClass
        
        "blur.uniform": ->
          divTag.removeClass options.focusClass
        
        "click.uniform touchend.uniform": ->
          unless $(elem).attr("checked")
            spanTag.removeClass options.checkedClass
          else
            spanTag.addClass options.checkedClass
        
        "mousedown.uniform touchbegin.uniform": ->
          divTag.addClass options.activeClass
        
        "mouseup.uniform touchend.uniform": ->
          divTag.removeClass options.activeClass
        
        "mouseenter.uniform": ->
          divTag.addClass options.hoverClass
        
        "mouseleave.uniform": ->
          divTag.removeClass options.hoverClass
          divTag.removeClass options.activeClass
      
      spanTag.addClass options.checkedClass  if $(elem).attr("checked")
      divTag.addClass options.disabledClass  if $(elem).attr("disabled")
      storeElement elem
    doRadio = (elem) ->
      $el = $(elem)
      divTag = $("<div />")
      spanTag = $("<span />")
      divTag.hide()  if not $el.css("display") == "none" and options.autoHide
      divTag.addClass options.radioClass
      divTag.attr "id", options.idPrefix + "-" + elem.attr("id")  if options.useID and elem.attr("id") != ""
      $(elem).wrap divTag
      $(elem).wrap spanTag
      spanTag = elem.parent()
      divTag = spanTag.parent()
      $(elem).css("opacity", 0).bind 
        "focus.uniform": ->
          divTag.addClass options.focusClass
        
        "blur.uniform": ->
          divTag.removeClass options.focusClass
        
        "click.uniform touchend.uniform": ->
          unless $(elem).attr("checked")
            spanTag.removeClass options.checkedClass
          else
            classes = options.radioClass.split(" ")[0]
            $("." + classes + " span." + options.checkedClass + ":has([name='" + $(elem).attr("name") + "'])").removeClass options.checkedClass
            spanTag.addClass options.checkedClass
        
        "mousedown.uniform touchend.uniform": ->
          divTag.addClass options.activeClass  unless $(elem).is(":disabled")
        
        "mouseup.uniform touchbegin.uniform": ->
          divTag.removeClass options.activeClass
        
        "mouseenter.uniform touchend.uniform": ->
          divTag.addClass options.hoverClass
        
        "mouseleave.uniform": ->
          divTag.removeClass options.hoverClass
          divTag.removeClass options.activeClass
      
      spanTag.addClass options.checkedClass  if $(elem).attr("checked")
      divTag.addClass options.disabledClass  if $(elem).attr("disabled")
      storeElement elem
    doFile = (elem) ->
      $el = $(elem)
      divTag = $("<div />")
      filenameTag = $("<span>" + options.fileDefaultText + "</span>")
      btnTag = $("<span>" + options.fileBtnText + "</span>")
      divTag.hide()  if not $el.css("display") == "none" and options.autoHide
      divTag.addClass options.fileClass
      filenameTag.addClass options.filenameClass
      btnTag.addClass options.fileBtnClass
      divTag.attr "id", options.idPrefix + "-" + $el.attr("id")  if options.useID and $el.attr("id") != ""
      $el.wrap divTag
      $el.after btnTag
      $el.after filenameTag
      divTag = $el.closest("div")
      filenameTag = $el.siblings("." + options.filenameClass)
      btnTag = $el.siblings("." + options.fileBtnClass)
      unless $el.attr("size")
        divWidth = divTag.width()
        $el.attr "size", divWidth / 10
      setFilename = ->
        filename = $el.val()
        if filename == ""
          filename = options.fileDefaultText
        else
          filename = filename.split(/[\/\\]+/)
          filename = filename[(filename.length - 1)]
        filenameTag.text filename
      
      setFilename()
      $el.css("opacity", 0).bind 
        "focus.uniform": ->
          divTag.addClass options.focusClass
        
        "blur.uniform": ->
          divTag.removeClass options.focusClass
        
        "mousedown.uniform": ->
          divTag.addClass options.activeClass  unless $(elem).is(":disabled")
        
        "mouseup.uniform": ->
          divTag.removeClass options.activeClass
        
        "mouseenter.uniform": ->
          divTag.addClass options.hoverClass
        
        "mouseleave.uniform": ->
          divTag.removeClass options.hoverClass
          divTag.removeClass options.activeClass
      
      if $.browser.msie
        $el.bind "click.uniform.ie7", ->
          setTimeout setFilename, 0
      else
        $el.bind "change.uniform", setFilename
      divTag.addClass options.disabledClass  if $el.attr("disabled")
      $.uniform.noSelect filenameTag
      $.uniform.noSelect btnTag
      storeElement elem
    storeElement = (elem) ->
      elem = $(elem).get()
      if elem.length > 1
        $.each elem, (i, val) ->
          $.uniform.elements.push val
      else
        $.uniform.elements.push elem
    options = $.extend($.uniform.options, options)
    el = this
    unless options.resetSelector == false
      $(options.resetSelector).mouseup ->
        resetThis = ->
          $.uniform.update el
        setTimeout resetThis, 10
    $.uniform.restore = (elem) ->
      elem = $($.uniform.elements)  if elem == undefined
      $(elem).each ->
        if $(this).is(":checkbox")
          $(this).unwrap().unwrap()
        else if $(this).is("select")
          $(this).siblings("span").remove()
          $(this).unwrap()
        else if $(this).is(":radio")
          $(this).unwrap().unwrap()
        else if $(this).is(":file")
          $(this).siblings("span").remove()
          $(this).unwrap()
        else $(this).unwrap().unwrap()  if $(this).is("button, :submit, :reset, a, input[type='button']")
        $(this).unbind ".uniform"
        $(this).css "opacity", "1"
        index = $.inArray($(elem), $.uniform.elements)
        $.uniform.elements.splice index, 1
    
    $.uniform.noSelect = (elem) ->
      f = ->
        false
      $(elem).each ->
        @onselectstart = @ondragstart = f
        $(this).mousedown(f).css MozUserSelect: "none"
    
    $.uniform.update = (elem) ->
      elem = $($.uniform.elements)  if elem == undefined
      elem = $(elem)
      elem.each ->
        $e = $(this)
        if $e.is("select")
          spanTag = $e.siblings("span")
          divTag = $e.parent("div")
          divTag.removeClass options.hoverClass + " " + options.focusClass + " " + options.activeClass
          spanTag.html $e.find(":selected").html()
          if $e.is(":disabled")
            divTag.addClass options.disabledClass
          else
            divTag.removeClass options.disabledClass
        else if $e.is(":checkbox")
          spanTag = $e.closest("span")
          divTag = $e.closest("div")
          divTag.removeClass options.hoverClass + " " + options.focusClass + " " + options.activeClass
          spanTag.removeClass options.checkedClass
          spanTag.addClass options.checkedClass  if $e.is(":checked")
          if $e.is(":disabled")
            divTag.addClass options.disabledClass
          else
            divTag.removeClass options.disabledClass
        else if $e.is(":radio")
          spanTag = $e.closest("span")
          divTag = $e.closest("div")
          divTag.removeClass options.hoverClass + " " + options.focusClass + " " + options.activeClass
          spanTag.removeClass options.checkedClass
          spanTag.addClass options.checkedClass  if $e.is(":checked")
          if $e.is(":disabled")
            divTag.addClass options.disabledClass
          else
            divTag.removeClass options.disabledClass
        else if $e.is(":file")
          divTag = $e.parent("div")
          filenameTag = $e.siblings(options.filenameClass)
          btnTag = $e.siblings(options.fileBtnClass)
          divTag.removeClass options.hoverClass + " " + options.focusClass + " " + options.activeClass
          filenameTag.text $e.val()
          if $e.is(":disabled")
            divTag.addClass options.disabledClass
          else
            divTag.removeClass options.disabledClass
        else if $e.is(":submit") or $e.is(":reset") or $e.is("button") or $e.is("a") or elem.is("input[type=button]")
          divTag = $e.closest("div")
          divTag.removeClass options.hoverClass + " " + options.focusClass + " " + options.activeClass
          if $e.is(":disabled")
            divTag.addClass options.disabledClass
          else
            divTag.removeClass options.disabledClass
    
    @each ->
      if $.support.selectOpacity
        elem = $(this)
        if elem.is("select")
          doSelect elem  if elem.attr("size") == undefined or elem.attr("size") <= 1  unless elem.attr("multiple") == true
        else if elem.is(":checkbox")
          doCheckbox elem
        else if elem.is(":radio")
          doRadio elem
        else if elem.is(":file")
          doFile elem
        else if elem.is(":text, :password, input[type='email']")
          doInput elem
        else if elem.is("textarea")
          doTextarea elem
        else doButton elem  if elem.is("a") or elem.is(":submit") or elem.is(":reset") or elem.is("button") or elem.is("input[type=button]")
) jQuery