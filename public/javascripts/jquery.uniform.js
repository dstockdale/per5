(function($) {
  $.uniform = {
    options: {
      selectClass: "selector",
      radioClass: "radio",
      checkboxClass: "checker",
      fileClass: "uploader",
      filenameClass: "filename",
      fileBtnClass: "action",
      fileDefaultText: "No file selected",
      fileBtnText: "Choose File",
      checkedClass: "checked",
      focusClass: "focus",
      disabledClass: "disabled",
      buttonClass: "button",
      activeClass: "active",
      hoverClass: "hover",
      useID: true,
      idPrefix: "uniform",
      resetSelector: false,
      autoHide: true
    },
    elements: []
  };
  if ($.browser.msie && $.browser.version < 7) {
    $.support.selectOpacity = false;
  } else {
    $.support.selectOpacity = true;
  }
  return $.fn.uniform = function(options) {
    var doButton, doCheckbox, doFile, doInput, doRadio, doSelect, doTextarea, el, storeElement;
    doInput = function(elem) {
      var $el;
      $el = $(elem);
      $el.addClass($el.attr("type"));
      return storeElement(elem);
    };
    doTextarea = function(elem) {
      $(elem).addClass("uniform");
      return storeElement(elem);
    };
    doButton = function(elem) {
      var $el, btnText, divTag, spanTag;
      $el = $(elem);
      divTag = $("<div>");
      spanTag = $("<span>");
      divTag.addClass(options.buttonClass);
      if (options.useID && $el.attr("id") !== "") {
        divTag.attr("id", options.idPrefix + "-" + $el.attr("id"));
      }
      if ($el.is("a") || $el.is("button")) {
        btnText = $el.text();
      } else {
        if ($el.is(":submit") || $el.is(":reset") || $el.is("input[type=button]")) {
          btnText = $el.attr("value");
        }
      }
      btnText = (btnText === "" ? ($el.is(":reset") ? "Reset" : "Submit") : btnText);
      spanTag.html(btnText);
      $el.css("opacity", 0);
      $el.wrap(divTag);
      $el.wrap(spanTag);
      divTag = $el.closest("div");
      spanTag = $el.closest("span");
      if ($el.is(":disabled")) {
        divTag.addClass(options.disabledClass);
      }
      divTag.bind({
        "mouseenter.uniform": function() {
          return divTag.addClass(options.hoverClass);
        },
        "mouseleave.uniform": function() {
          divTag.removeClass(options.hoverClass);
          return divTag.removeClass(options.activeClass);
        },
        "mousedown.uniform touchbegin.uniform": function() {
          return divTag.addClass(options.activeClass);
        },
        "mouseup.uniform touchend.uniform": function() {
          return divTag.removeClass(options.activeClass);
        },
        "click.uniform touchend.uniform": function(e) {
          var ev;
          if ($(e.target).is("span") || $(e.target).is("div")) {
            if (elem[0].dispatchEvent) {
              ev = document.createEvent("MouseEvents");
              ev.initEvent("click", true, true);
              return elem[0].dispatchEvent(ev);
            } else {
              return elem[0].click();
            }
          }
        }
      });
      elem.bind({
        "focus.uniform": function() {
          return divTag.addClass(options.focusClass);
        },
        "blur.uniform": function() {
          return divTag.removeClass(options.focusClass);
        }
      });
      $.uniform.noSelect(divTag);
      return storeElement(elem);
    };
    doSelect = function(elem) {
      var $el, divTag, selected, spanTag;
      $el = $(elem);
      divTag = $("<div />");
      spanTag = $("<span />");
      if (!$el.css("display") === "none" && options.autoHide) {
        divTag.hide();
      }
      divTag.addClass(options.selectClass);
      if (options.useID && elem.attr("id") !== "") {
        divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
      }
      selected = elem.find(":selected:first");
      if (selected.length === 0) {
        selected = elem.find("option:first");
      }
      spanTag.html(selected.html());
      elem.css("opacity", 0);
      elem.wrap(divTag);
      elem.before(spanTag);
      divTag = elem.parent("div");
      spanTag = elem.siblings("span");
      elem.bind({
        "change.uniform": function() {
          spanTag.text(elem.find(":selected").html());
          return divTag.removeClass(options.activeClass);
        },
        "focus.uniform": function() {
          return divTag.addClass(options.focusClass);
        },
        "blur.uniform": function() {
          divTag.removeClass(options.focusClass);
          return divTag.removeClass(options.activeClass);
        },
        "mousedown.uniform touchbegin.uniform": function() {
          return divTag.addClass(options.activeClass);
        },
        "mouseup.uniform touchend.uniform": function() {
          return divTag.removeClass(options.activeClass);
        },
        "click.uniform touchend.uniform": function() {
          return divTag.removeClass(options.activeClass);
        },
        "mouseenter.uniform": function() {
          return divTag.addClass(options.hoverClass);
        },
        "mouseleave.uniform": function() {
          divTag.removeClass(options.hoverClass);
          return divTag.removeClass(options.activeClass);
        },
        "keyup.uniform": function() {
          return spanTag.text(elem.find(":selected").html());
        }
      });
      if ($(elem).attr("disabled")) {
        divTag.addClass(options.disabledClass);
      }
      $.uniform.noSelect(spanTag);
      return storeElement(elem);
    };
    doCheckbox = function(elem) {
      var $el, divTag, spanTag;
      $el = $(elem);
      divTag = $("<div />");
      spanTag = $("<span />");
      if (!$el.css("display") === "none" && options.autoHide) {
        divTag.hide();
      }
      divTag.addClass(options.checkboxClass);
      if (options.useID && elem.attr("id") !== "") {
        divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
      }
      $(elem).wrap(divTag);
      $(elem).wrap(spanTag);
      spanTag = elem.parent();
      divTag = spanTag.parent();
      $(elem).css("opacity", 0).bind({
        "focus.uniform": function() {
          return divTag.addClass(options.focusClass);
        },
        "blur.uniform": function() {
          return divTag.removeClass(options.focusClass);
        },
        "click.uniform touchend.uniform": function() {
          if (!$(elem).attr("checked")) {
            return spanTag.removeClass(options.checkedClass);
          } else {
            return spanTag.addClass(options.checkedClass);
          }
        },
        "mousedown.uniform touchbegin.uniform": function() {
          return divTag.addClass(options.activeClass);
        },
        "mouseup.uniform touchend.uniform": function() {
          return divTag.removeClass(options.activeClass);
        },
        "mouseenter.uniform": function() {
          return divTag.addClass(options.hoverClass);
        },
        "mouseleave.uniform": function() {
          divTag.removeClass(options.hoverClass);
          return divTag.removeClass(options.activeClass);
        }
      });
      if ($(elem).attr("checked")) {
        spanTag.addClass(options.checkedClass);
      }
      if ($(elem).attr("disabled")) {
        divTag.addClass(options.disabledClass);
      }
      return storeElement(elem);
    };
    doRadio = function(elem) {
      var $el, divTag, spanTag;
      $el = $(elem);
      divTag = $("<div />");
      spanTag = $("<span />");
      if (!$el.css("display") === "none" && options.autoHide) {
        divTag.hide();
      }
      divTag.addClass(options.radioClass);
      if (options.useID && elem.attr("id") !== "") {
        divTag.attr("id", options.idPrefix + "-" + elem.attr("id"));
      }
      $(elem).wrap(divTag);
      $(elem).wrap(spanTag);
      spanTag = elem.parent();
      divTag = spanTag.parent();
      $(elem).css("opacity", 0).bind({
        "focus.uniform": function() {
          return divTag.addClass(options.focusClass);
        },
        "blur.uniform": function() {
          return divTag.removeClass(options.focusClass);
        },
        "click.uniform touchend.uniform": function() {
          var classes;
          if (!$(elem).attr("checked")) {
            return spanTag.removeClass(options.checkedClass);
          } else {
            classes = options.radioClass.split(" ")[0];
            $("." + classes + " span." + options.checkedClass + ":has([name='" + $(elem).attr("name") + "'])").removeClass(options.checkedClass);
            return spanTag.addClass(options.checkedClass);
          }
        },
        "mousedown.uniform touchend.uniform": function() {
          if (!$(elem).is(":disabled")) {
            return divTag.addClass(options.activeClass);
          }
        },
        "mouseup.uniform touchbegin.uniform": function() {
          return divTag.removeClass(options.activeClass);
        },
        "mouseenter.uniform touchend.uniform": function() {
          return divTag.addClass(options.hoverClass);
        },
        "mouseleave.uniform": function() {
          divTag.removeClass(options.hoverClass);
          return divTag.removeClass(options.activeClass);
        }
      });
      if ($(elem).attr("checked")) {
        spanTag.addClass(options.checkedClass);
      }
      if ($(elem).attr("disabled")) {
        divTag.addClass(options.disabledClass);
      }
      return storeElement(elem);
    };
    doFile = function(elem) {
      var $el, btnTag, divTag, divWidth, filenameTag, setFilename;
      $el = $(elem);
      divTag = $("<div />");
      filenameTag = $("<span>" + options.fileDefaultText + "</span>");
      btnTag = $("<span>" + options.fileBtnText + "</span>");
      if (!$el.css("display") === "none" && options.autoHide) {
        divTag.hide();
      }
      divTag.addClass(options.fileClass);
      filenameTag.addClass(options.filenameClass);
      btnTag.addClass(options.fileBtnClass);
      if (options.useID && $el.attr("id") !== "") {
        divTag.attr("id", options.idPrefix + "-" + $el.attr("id"));
      }
      $el.wrap(divTag);
      $el.after(btnTag);
      $el.after(filenameTag);
      divTag = $el.closest("div");
      filenameTag = $el.siblings("." + options.filenameClass);
      btnTag = $el.siblings("." + options.fileBtnClass);
      if (!$el.attr("size")) {
        divWidth = divTag.width();
        $el.attr("size", divWidth / 10);
      }
      setFilename = function() {
        var filename;
        filename = $el.val();
        if (filename === "") {
          filename = options.fileDefaultText;
        } else {
          filename = filename.split(/[\/\\]+/);
          filename = filename[filename.length - 1];
        }
        return filenameTag.text(filename);
      };
      setFilename();
      $el.css("opacity", 0).bind({
        "focus.uniform": function() {
          return divTag.addClass(options.focusClass);
        },
        "blur.uniform": function() {
          return divTag.removeClass(options.focusClass);
        },
        "mousedown.uniform": function() {
          if (!$(elem).is(":disabled")) {
            return divTag.addClass(options.activeClass);
          }
        },
        "mouseup.uniform": function() {
          return divTag.removeClass(options.activeClass);
        },
        "mouseenter.uniform": function() {
          return divTag.addClass(options.hoverClass);
        },
        "mouseleave.uniform": function() {
          divTag.removeClass(options.hoverClass);
          return divTag.removeClass(options.activeClass);
        }
      });
      if ($.browser.msie) {
        $el.bind("click.uniform.ie7", function() {
          return setTimeout(setFilename, 0);
        });
      } else {
        $el.bind("change.uniform", setFilename);
      }
      if ($el.attr("disabled")) {
        divTag.addClass(options.disabledClass);
      }
      $.uniform.noSelect(filenameTag);
      $.uniform.noSelect(btnTag);
      return storeElement(elem);
    };
    storeElement = function(elem) {
      elem = $(elem).get();
      if (elem.length > 1) {
        return $.each(elem, function(i, val) {
          return $.uniform.elements.push(val);
        });
      } else {
        return $.uniform.elements.push(elem);
      }
    };
    options = $.extend($.uniform.options, options);
    el = this;
    if (options.resetSelector !== false) {
      $(options.resetSelector).mouseup(function() {
        var resetThis;
        resetThis = function() {
          return $.uniform.update(el);
        };
        return setTimeout(resetThis, 10);
      });
    }
    $.uniform.restore = function(elem) {
      if (elem === void 0) {
        elem = $($.uniform.elements);
      }
      return $(elem).each(function() {
        var index;
        if ($(this).is(":checkbox")) {
          $(this).unwrap().unwrap();
        } else if ($(this).is("select")) {
          $(this).siblings("span").remove();
          $(this).unwrap();
        } else if ($(this).is(":radio")) {
          $(this).unwrap().unwrap();
        } else if ($(this).is(":file")) {
          $(this).siblings("span").remove();
          $(this).unwrap();
        } else {
          if ($(this).is("button, :submit, :reset, a, input[type='button']")) {
            $(this).unwrap().unwrap();
          }
        }
        $(this).unbind(".uniform");
        $(this).css("opacity", "1");
        index = $.inArray($(elem), $.uniform.elements);
        return $.uniform.elements.splice(index, 1);
      });
    };
    $.uniform.noSelect = function(elem) {
      var f;
      f = function() {
        return false;
      };
      return $(elem).each(function() {
        this.onselectstart = this.ondragstart = f;
        return $(this).mousedown(f).css({
          MozUserSelect: "none"
        });
      });
    };
    $.uniform.update = function(elem) {
      if (elem === void 0) {
        elem = $($.uniform.elements);
      }
      elem = $(elem);
      return elem.each(function() {
        var $e, btnTag, divTag, filenameTag, spanTag;
        $e = $(this);
        if ($e.is("select")) {
          spanTag = $e.siblings("span");
          divTag = $e.parent("div");
          divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
          spanTag.html($e.find(":selected").html());
          if ($e.is(":disabled")) {
            return divTag.addClass(options.disabledClass);
          } else {
            return divTag.removeClass(options.disabledClass);
          }
        } else if ($e.is(":checkbox")) {
          spanTag = $e.closest("span");
          divTag = $e.closest("div");
          divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
          spanTag.removeClass(options.checkedClass);
          if ($e.is(":checked")) {
            spanTag.addClass(options.checkedClass);
          }
          if ($e.is(":disabled")) {
            return divTag.addClass(options.disabledClass);
          } else {
            return divTag.removeClass(options.disabledClass);
          }
        } else if ($e.is(":radio")) {
          spanTag = $e.closest("span");
          divTag = $e.closest("div");
          divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
          spanTag.removeClass(options.checkedClass);
          if ($e.is(":checked")) {
            spanTag.addClass(options.checkedClass);
          }
          if ($e.is(":disabled")) {
            return divTag.addClass(options.disabledClass);
          } else {
            return divTag.removeClass(options.disabledClass);
          }
        } else if ($e.is(":file")) {
          divTag = $e.parent("div");
          filenameTag = $e.siblings(options.filenameClass);
          btnTag = $e.siblings(options.fileBtnClass);
          divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
          filenameTag.text($e.val());
          if ($e.is(":disabled")) {
            return divTag.addClass(options.disabledClass);
          } else {
            return divTag.removeClass(options.disabledClass);
          }
        } else if ($e.is(":submit") || $e.is(":reset") || $e.is("button") || $e.is("a") || elem.is("input[type=button]")) {
          divTag = $e.closest("div");
          divTag.removeClass(options.hoverClass + " " + options.focusClass + " " + options.activeClass);
          if ($e.is(":disabled")) {
            return divTag.addClass(options.disabledClass);
          } else {
            return divTag.removeClass(options.disabledClass);
          }
        }
      });
    };
    return this.each(function() {
      var elem;
      if ($.support.selectOpacity) {
        elem = $(this);
        if (elem.is("select")) {
          if (elem.attr("multiple") !== true ? elem.attr("size") === void 0 || elem.attr("size") <= 1 : void 0) {
            return doSelect(elem);
          }
        } else if (elem.is(":checkbox")) {
          return doCheckbox(elem);
        } else if (elem.is(":radio")) {
          return doRadio(elem);
        } else if (elem.is(":file")) {
          return doFile(elem);
        } else if (elem.is(":text, :password, input[type='email']")) {
          return doInput(elem);
        } else if (elem.is("textarea")) {
          return doTextarea(elem);
        } else {
          if (elem.is("a") || elem.is(":submit") || elem.is(":reset") || elem.is("button") || elem.is("input[type=button]")) {
            return doButton(elem);
          }
        }
      }
    });
  };
})(jQuery);