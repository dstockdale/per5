var add_image_handlers;
add_image_handlers = function() {
  $("#main-image").data("selectedThumb", $("#main-image img").attr("src"));
  $("ul.thumbnails li").eq(0).addClass("selected");
  return $("ul.thumbnails li a").click(function() {
    $("#main-image").data("selectedThumb", $(this).attr("href"));
    $("ul.thumbnails li").removeClass("selected");
    $(this).parent("li").addClass("selected");
    return false;
  }).hover((function() {
    $("#main-image img").attr("src", $(this).attr("href").replace("mini", "product"));
  }), function() {
    $("#main-image img").attr("src", $("#main-image").data("selectedThumb"));
  });
};
jQuery(document).ready(function() {
  add_image_handlers();
});
jQuery(document).ready(function() {
  var styleColours;
  window.shirtStyleTemplate = '<div class="shirt_styles_section">\n  <div class="shirt_styles_title">\n    Choose Style\n  </div>\n  <div id="shirt_styles" class="shirt_styles shirtStyle_white">\n    <div id="vest" class="vest selected"></div>\n    <div id="racer_back" class="racer_back"></div>\n    <div id="half_sleeve" class="half_sleeve"></div>\n  </div>\n</div>';
  window.shirtStyles = {};
  window.shirtColours = {};
  window.shirtColoursArr = [];
  if (jQuery("#shirt_styles").length < 1) {
    jQuery("#option-type_shirt_style_womens").before(window.shirtStyleTemplate);
  }
  $('#option-type_shirt_style_womens option').each(function(i, item) {
    var $item;
    $item = $(item);
    window.shirtStyles[$item.text().toLowerCase().replace(/\s/g, '_')] = $item.val();
  });
  $('div[id^="option-type_colour_"]').find('option').each(function(i, item) {
    var $item, string, value;
    $item = $(item);
    string = $item.text().toLowerCase().split(' ')[0];
    value = $item.val();
    window.shirtColours[string] = value;
    window.shirtColoursArr[value] = string;
  });
  styleColours = function() {
    var selectedColour;
    selectedColour = 'shirtStyle_' + window.shirtColoursArr[$('div[id^="option-type_colour_"]').find('option:selected').val()];
    $('#shirt_styles').removeClass('shirtStyle_white shirtStyle_black').addClass(selectedColour);
  };
  $('div[id^="option-type_colour_"]').find('select').change(function() {
    styleColours();
  });
  styleColours();
  $('#shirt_styles > div').click(function() {
    var choice;
    $(this).siblings().removeClass('selected');
    $(this).toggleClass('selected');
    choice = $(this).attr('id');
    $('#option-type_shirt_style_womens select').val(window.shirtStyles[choice]);
    $('#option-type_shirt_style_womens').change();
  });
  return jQuery("#product-variants input[type=radio]").click(function(event) {
    var link, text, vid;
    vid = this.value;
    text = $(this).siblings(".variant-description").html();
    jQuery("#variant-thumbnails").empty();
    jQuery("#variant-images span").html(text);
    if (images[vid].length > 0) {
      $.each(images[vid], function(i, link) {
        jQuery("#variant-thumbnails").append("<li>" + link + "</li>");
      });
      jQuery("#variant-images").show();
    } else {
      jQuery("#variant-images").hide();
    }
    add_image_handlers();
    link = jQuery("#variant-thumbnails a")[0];
    jQuery("#main-image img").attr({
      src: jQuery(link).attr("href")
    });
    jQuery("ul.thumbnails li").removeClass("selected");
    return jQuery(link).parent("li").addClass("selected");
  });
});