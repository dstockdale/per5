(function($) {
  $(document).ready(function() {
    $('#banner').waitForImages(function() {
      return $(this).removeClass('hideArrows');
    });
    $("#showcase").awShowcase({
      content_width: 955,
      content_height: 500,
      buttons: false,
      auto: true,
      pauseonover: true,
      interval: 3000,
      continuous: true,
      stoponclick: false,
      transition_speed: 800
    });
    $.facebox.settings.closeImage = '/images/facebox/closelabel.png';
    $.facebox.settings.loadingImage = '/images/facebox/loading.gif';
    $(document).bind('reveal.facebox', function() {
      var amount;
      amount = $('p.prices span.price').text();
      $('#facebox').find('select,button,input').uniform();
      $('#convertAmount').text(amount);
      $('#closeFacebox').live('click', function() {
        return $(document).trigger('close.facebox');
      });
      return $('#submitAddToCart').live('click', function() {
        return $('#cart-form .addToCartButton').click();
      });
    });
    $('a[rel=facebox]').facebox();
    $('.currencyLookup').click(function(e) {
      var amount;
      e.preventDefault();
      amount = $('p.prices').find('span.price').text();
      return $.facebox({
        ajax: '/currencies.html'
      });
    });
    $('.navigation-list > li a').dblclick(function(e) {
      e.stopPropagation();
      return window.document.location.href = $(this).attr('href');
    });
    $('.navigation-list > li h3').click(function(e) {
      e.preventDefault();
      $(this).parent('li').siblings('li').removeClass('active').find('ul').slideUp().removeClass('active');
      return $(this).parent('li').find('ul').slideDown();
    });
    $('#convert').live("click", function(e) {
      var amount, currency;
      e.preventDefault();
      currency = $('#toCurrency').val();
      amount = $('#convertAmount').text();
      return $.ajax({
        url: "/exchange",
        type: 'get',
        dataType: 'json',
        data: "currency=" + currency + "&amount=" + amount,
        beforeSend: function() {
          return $('#coversionResult').html("<span class='beforeSend'>Fetching rates from http://www.google.com/finance/converter</span>");
        },
        success: function(data) {
          var response;
          response = "<span class='convertedPrice'>" + data.converted + " " + data.rhs + "</span>";
          return $('#coversionResult').html(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
          return $('#coversionResult').text("Oops there was an error! Please try clicking 'Convert' again. Sometimes the server might be busy.");
        }
      });
    });
    $('#messages').find('input, button, textarea').uniform();
    $('#checkout_form_registration').find('input, button').uniform();
    $('#cart-form').find('select,input').not('#option_values_12_3').uniform();
    $('#billing input, #shipping input, #order_submit').uniform();
    $('#login, #cart').find('input,button,a.button').uniform();
    if ($('#flashMessage,#flashErrorMessage').length > 0) {
      setTimeout(function() {
        return $('#flashMessage,#flashErrorMessage').css({
          'overflow': 'hidden'
        }).animate({
          width: 120,
          height: 120,
          'margin-left': "+=115"
        }, 2000).animate({
          height: 10
        }, 1000).fadeOut();
      }, 5000);
    }
    $("form#updatecart a.delete").show().live("click", function(e) {
      $(this).parents("tr").find("input.line_item_quantity").val(0);
      $(this).parents("form").submit();
      e.preventDefault();
    });
  });
})(jQuery);