class AddShowcaseFieldsToAssets < ActiveRecord::Migration
  def self.up
    add_column :assets, :active, :boolean, :default => false
  end

  def self.down
    remove_column :assets, :column_name
    remove_column :assets, :active
  end
end
