class AddNavTitleToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :nav_title, :string
  end

  def self.down
    remove_column :products, :nav_title
  end
end
