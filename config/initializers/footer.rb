class CustomizeFooterHook < Spree::ThemeSupport::HookListener
  replace :footer_left do
    %Q{ &copy; <%= Date.today.year %> PER5 London. All rights reserved }
  end
end