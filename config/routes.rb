Per5::Application.routes.draw do
  resource :messages
  root :to => "homepage#show"
  match '/contact' => 'messages#new'
  match '/thankyou' => 'messages#show'
  match "/exchange" => "exchanges#show"
end