@europe =  ["Albania","Andorra","Armenia","Austria","Azerbaijan","Azores","Balearic Islands","Belarus","Belgium","Bosnia Herzegovina","Bulgaria","Canary Islands","Corsica","Croatia","Cyprus","Czech Republic","Denmark","Estonia","Faroe Islands","Finland","France","Georgia","Germany","Gibraltar","Greece","Greenland","Hungary","Iceland","Irish Republic","Italy","Kazakhstan","Kosovo","Kyrgyzstan","Latvia","Liechtenstein","Lithuania","Luxembourg","Macedonia","Madeira","Malta","Moldova","Monaco","Montenegro","Netherlands","Norway","Poland","Portugal","Romania","Russia","San Marino","Serbia","Slovakia","Slovenia","Spain","Sweden","Switzerland","Tajikistan","Turkey","Turkmenistan","Ukraine","Uzbekistan","Vatican City State"]

namespace :per5 do

  desc "Last commit id"
  task :last_commit do 
    puts last_commit
  end

  desc "Upload Jammit assets to Amazon S3 and set RAILS_ASSET_ID on heroku to the last git commit"
  task :upload, :app do |t, args|
    puts "Warning! your git directory is not clean." unless git_status.empty?
    asset_version = last_commit
    ENV["RAILS_ASSET_ID"] = asset_version
    set_heroku_var_cmd = "heroku config:add RAILS_ASSET_ID=#{asset_version}"
    set_heroku_var_cmd << " --app #{args[:app]}" unless args[:app].nil? or args[:app].empty? # no Rails loaded for .blank?/present?
    system(set_heroku_var_cmd) or fail("Could not set RAILS_ASSET_ID")
  end
  
  desc "Destroy test orders, line items"
  task :destroy_test_data => :environment do
    Order.connection.execute("TRUNCATE orders")
    Order.connection.execute("TRUNCATE line_items")
    Order.connection.execute("TRUNCATE payments")
    Order.connection.execute("TRUNCATE shipments")
    puts "Order count: #{Order.all.size}"
    puts "LineItem count: #{LineItem.all.size}"
    puts "Payment count: #{Payment.all.size}"
    puts "Shipment count: #{Shipment.all.size}"
  end
  
  desc "Rebuild images"
  task :rebuild_images => :environment do
    Product.active.each do |product|
      product.images.each do |image|
        image.attachment.reprocess!
      end
    end
  end

  desc "Create countries for European Zone"
  task :eurozone => :environment do
    n = 0
    missing = "\n ><{{{*>  Missing countries  <*}}}><\n\n"
    @europe.each do |euro|
      country = Country.where("name LIKE ?","%#{euro}%").limit(1).first
      if !country.blank?
        zone_member = ZoneMember.find_by_zone_id_and_zoneable_id_and_zoneable_type(4,country.id,"Country")
        unless zone_member
          ZoneMember.create(:zone_id => 4, :zoneable_id => country.id, :zoneable_type => "Country")
          puts "Adding #{country.name} to Europe zone (5)"
        end
      else
        missing+="Missing: #{euro}\n"
      end
      n+=1
    end
    puts "#{missing}"
    puts "#{n} countries"
  end
  
  desc "Reset ZoneMembers"
  task :reset_zones => :environment do
    ZoneMember.find_all_by_zone_id([4,5]).each(&:destroy)
  end
  
  desc "Create countries for Rest of the World Zone"
  task :worldzone => :environment do
    eu = []
    @europe << "United Kingdom"
    @europe.each do |euro|
      country = Country.select(:id).where("name LIKE ?","%#{euro}%").limit(1).first
      if country
        eu << country.id
      end
    end
    rest_of_world = Country.select([:id,:name]).where("id NOT IN(#{eu.join(",")})")
    rest_of_world.each do |row|
      zone_member = ZoneMember.find_by_zone_id_and_zoneable_id_and_zoneable_type(5,row.id,"Country")
      if zone_member == nil
        ZoneMember.create(:zone_id => 5, :zoneable_id => row.id, :zoneable_type => "Country")
        puts "Adding #{row.name} [#{row.id}] to ROW zone (5)"
      end
    end
    
  end
  
  desc "Set all the Variants to have 99999 in stock"
  task :fill_stock => :environment do
    Variant.update_all :count_on_hand => 99999
  end

end

def last_commit
  `git rev-parse HEAD`.chomp[0..6]
end

def git_status
  `git status --porcelain`.chomp
end
